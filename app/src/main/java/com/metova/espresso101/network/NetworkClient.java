package com.metova.espresso101.network;

public interface NetworkClient {

    String getString();

    void getString(Callback callback);

    interface Callback {

        void setString(String text);
    }
}