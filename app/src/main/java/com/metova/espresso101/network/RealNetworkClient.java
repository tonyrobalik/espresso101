package com.metova.espresso101.network;

import android.support.annotation.NonNull;

public class RealNetworkClient implements NetworkClient {

    private static final String mText = "this is the real text";

    @NonNull
    @Override
    public String getString() {
        return mText;
    }

    @Override
    public void getString(@NonNull Callback callback) {
        callback.setString(mText);
    }
}