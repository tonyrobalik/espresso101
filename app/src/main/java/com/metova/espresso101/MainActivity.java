package com.metova.espresso101;

import com.metova.espresso101.network.NetworkClient;
import com.metova.espresso101.network.RealNetworkClient;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mTextHello;

    private NetworkClient mNetworkClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextHello = (TextView) findViewById(R.id.text_hello);

        mNetworkClient = new RealNetworkClient();
    }

    public void onClick(View view) {
        mNetworkClient.getString(new NetworkClient.Callback() {
            @Override
            public void setString(String text) {
                mTextHello.setText(text);
            }
        });
    }

    @VisibleForTesting
    public void setNetworkClient(@NonNull NetworkClient networkClient) {
        mNetworkClient = networkClient;
    }
}