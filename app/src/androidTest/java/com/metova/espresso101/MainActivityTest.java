package com.metova.espresso101;

import com.metova.espresso101.network.NetworkClient;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import android.support.test.rule.ActivityTestRule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class, true, true);

    @Test
    public void testClickButtonShouldSetText() throws Exception {
        // Setup
        // Create mocks and behavior
        NetworkClient mockNetworkClient = mock(NetworkClient.class);
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                NetworkClient.Callback callback = (NetworkClient.Callback) invocation.getArguments()[0];
                callback.setString("this is the mock text");
                return null;
            }
        }).when(mockNetworkClient).getString(any(NetworkClient.Callback.class));
        // Inject mocked dependency
        mActivityTestRule.getActivity().setNetworkClient(mockNetworkClient);

        // Exercise
        onView(withId(R.id.button_click_me)).perform(click());

        // Verify
        verify(mockNetworkClient).getString(any(NetworkClient.Callback.class));
        onView(allOf(withId(R.id.text_hello), withText("this is the mock text")))
                .check(matches(isDisplayed()));
    }
}